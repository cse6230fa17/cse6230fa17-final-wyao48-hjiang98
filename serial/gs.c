#include <stdio.h>
#include "gs.h"

int gauss_seidel_iteration(size_t n, int n_iter, const double *f, double *u)
{
  int p, q;
  int i, j, k, res;
  int x, y, z;
  double h = 1./n;
  double a1, a2, a3;
  double temp1, temp2;

  a1 = 8./3 * h;
  a2 = -1./6 * h;
  a3 = -1./12 * h;

  //int edge[6] = { (n-1)*(n-1)+(n-1), (n-1)*(n-1)+1, (n-1)*(n-1)-1, (n-1)*(n-1)-(n-1), (n-1)+1, (n-1)-1 };
  //int corner[4] = { (n-1)*(n-1)+(n-1)+1, (n-1)*(n-1)+(n-1)-1, (n-1)*(n-1)-(n-1)+1, (n-1)*(n-1)-(n-1)-1 };

  for ( p = 0; p < n_iter; p++ ){
  	for ( q = 0; q < (n-1)*(n-1)*(n-1); q++ ){
      i = (q - q % ((n-1)*(n-1))) / ((n-1)*(n-1));
      res = q % ((n-1)*(n-1));
      j = (res - res % (n-1)) / (n-1);
      k = res % (n-1);

      temp1 = 0;
      for (x = -1; x <= 1; x+=2){
        for (y = -1; y <= 1; y+=2){
          if( (i+x)>=0 && (i+x)<(n-1) && (j+y)>=0 && (j+y)<(n-1) ) {
            temp1 += u[q + x*(n-1)*(n-1) + y*(n-1)];
          }
          if( (j+x)>=0 && (j+x)<(n-1) && (k+y)>=0 && (k+y)<(n-1) ) {
            temp1 += u[q + x*(n-1) + y];
          }
          if( (i+x)>=0 && (i+x)<(n-1) && (k+y)>=0 && (k+y)<(n-1) ) {
            temp1 += u[q + x*(n-1)*(n-1) + y];
          }
        }
      }
      temp1 *= a2;

      temp2 = 0;
      for (x = -1; x <= 1; x+=2){
        for (y = -1; y <= 1; y+=2){
          for (z = -1; z <= 1; z+=2){
            if( (i+x)>=0 && (i+x)<(n-1) && (j+y)>=0 && (j+y)<(n-1) && (k+z)>=0 && (k+z)<(n-1) ) {
              temp2 += u[q + x*(n-1)*(n-1) + y*(n-1) + z];
            }
          }
        }
      }
      temp2 *= a3;

  		u[q] = ( f[q] - temp1 - temp2 ) / a1;
  	}
  }

  return 0;
}

int gauss_seidel_ordering(size_t n, size_t *order)
{
  for (size_t i = 0; i < (n - 1) * (n - 1) * (n - 1); i++) {
    order[i] = i;
  }

  return 0;
}
