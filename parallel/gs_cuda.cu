
#include <petscsys.h>
#include <petscmat.h>
#include "gs.h"
#include <stdio.h>

#define CUDA_CHK(cerr) do {cudaError_t _cerr = (cerr); if ((_cerr) != cudaSuccess) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_LIB,"Cuda error");} while(0)

__global__ void updateKernel(size_t N, int color, const double *fLocal, double *u_local)
{
    N = N-1;
    int i_start = N/(gridDim.x-1)*blockIdx.x;
    int i_end = min(N/(gridDim.x-1)*(blockIdx.x+1),N);
    int j_start = (color>=2);
    int j_end = N;
    int k_start = N/(blockDim.x-1)*threadIdx.x;
    int k_end = min(N/(blockDim.x-1)*(threadIdx.x+1),N);
    if((color%2) != i_start%2)
      i_start++;

    // printf("thread_id:%d , block_id:%d \n i:%d->%d j:%d->%d k:%d->%d",
    // thread_id,block_id,
    // i_start,i_end,j_start,j_end,k_start,k_end);
    for(int i=i_start; i<i_end; i=i+2)
      for(int j=j_start; j<j_end; j=j+2)
        for(int k=k_start; k<k_end; k++)
        {
          bool i_max = (i<=(N-1)), i_min = (i>=0);
          bool j_max = (j<=(N-1)), j_min = (j>=0);
          bool k_max = (k<=(N-1)), k_min = (k>=0);
          // printf("%ld: i: %d,  j: %d,  k: %d  (thread_id:%d , block_id:%d)\n", i*N*N+j*N+k,i,j,k,thread_id,block_id);
          // printf("u:%lf\n",u_local[i*N*N+j*N+k]);
          // printf("f:%lf\n",fLocal[i*N*N+j*N+k]);
          // if(i*N*N+j*N+k >= N*N*N) printf("warning1!");
          // if(i >= N || j >= N || k >= N) printf("warning2!");
          u_local[i*N*N+j*N+k] = fLocal[i*N*N+j*N+k];
          double temp1 = 0;
          if(i_min*j_min*k_min) temp1+=u_local[(i-1)*N*N+(j-1)*N+k-1];
          if(i_min*j_min*k_max) temp1+=u_local[(i-1)*N*N+(j-1)*N+k+1];
          if(i_min*j_max*k_min) temp1+=u_local[(i-1)*N*N+(j+1)*N+k-1];
          if(i_min*j_max*k_max) temp1+=u_local[(i-1)*N*N+(j+1)*N+k+1];
          if(i_max*j_min*k_min) temp1+=u_local[(i+1)*N*N+(j-1)*N+k-1];
          if(i_max*j_min*k_max) temp1+=u_local[(i+1)*N*N+(j-1)*N+k+1];
          if(i_max*j_max*k_min) temp1+=u_local[(i+1)*N*N+(j+1)*N+k-1];
          if(i_max*j_max*k_max) temp1+=u_local[(i+1)*N*N+(j+1)*N+k+1];
          temp1 *= 1./12./(N+1);
          u_local[i*N*N+j*N+k] += temp1;
          temp1 = 0;
          if(j_min*k_min) temp1+=u_local[(i)*N*N+(j-1)*N+k-1];
          if(j_min*k_max) temp1+=u_local[(i)*N*N+(j-1)*N+k+1];
          if(j_max*k_min) temp1+=u_local[(i)*N*N+(j+1)*N+k-1];
          if(j_max*k_max) temp1+=u_local[(i)*N*N+(j+1)*N+k+1];
          if(i_min*k_min) temp1+=u_local[(i-1)*N*N+(j)*N+k-1];
          if(i_min*k_max) temp1+=u_local[(i-1)*N*N+(j)*N+k+1];
          if(i_max*k_min) temp1+=u_local[(i+1)*N*N+(j)*N+k-1];
          if(i_max*k_max) temp1+=u_local[(i+1)*N*N+(j)*N+k+1];
          if(i_min*j_min) temp1+=u_local[(i-1)*N*N+(j-1)*N+k];
          if(i_min*j_max) temp1+=u_local[(i-1)*N*N+(j+1)*N+k];
          if(i_max*j_min) temp1+=u_local[(i+1)*N*N+(j-1)*N+k];
          if(i_max*j_max) temp1+=u_local[(i+1)*N*N+(j+1)*N+k];
          temp1 *= 1./6./(N+1);
          u_local[i*N*N+j*N+k] += temp1;

          u_local[i*N*N+j*N+k]/=(8./3.)/(N+1);
        }

}

int GSGetFieldArraysDevice(GS gs, size_t N, int numDevices,
                           struct IndexBlock iBlock[],
                           double *fLocal_p[], double *uLocal_p[])
{
  cudaError_t       cerr;

  PetscFunctionBeginUser;

  iBlock[0].xStart = 0;
  iBlock[0].xEnd   = N - 1;
  iBlock[0].yStart = 0;
  iBlock[0].yEnd   = N - 1;
  iBlock[0].zStart = 0;
  iBlock[0].zEnd   = N - 1;
  cerr = cudaMalloc(&fLocal_p[0], (N-1) * (N-1) * (N-1) * sizeof(double)); CUDA_CHK(cerr);
  cerr = cudaMalloc(&uLocal_p[0], (N-1) * (N-1) * (N-1) * sizeof(double)); CUDA_CHK(cerr);

  for (int i = 1; i < numDevices; i++) {
    iBlock[i].xStart = N - 1;
    iBlock[i].xEnd   = N - 1;
    iBlock[i].yStart = N - 1;
    iBlock[i].yEnd   = N - 1;
    iBlock[i].zStart = N - 1;
    iBlock[i].zEnd   = N - 1;
    fLocal_p[i] = NULL;
    uLocal_p[i] = NULL;
  }

  PetscFunctionReturn(0);
}

int GSRestoreFieldArraysDevice(GS gs, size_t N, int numDevices,
                               struct IndexBlock iBlock[],
                               double *fLocal_p[], double *uLocal_p[])
{
  cudaError_t    cerr;

  PetscFunctionBeginUser;
  cerr = cudaFree(fLocal_p[0]); CUDA_CHK(cerr);
  cerr = cudaFree(uLocal_p[0]); CUDA_CHK(cerr);
  PetscFunctionReturn(0);
}

int GSIterateDevice(GS gs, size_t N, int num_iter, int numDevices,
                    const struct IndexBlock iBlock[],
                    const double *fLocal[], double *uLocal[])
{
  cudaError_t    cerr;
  PetscFunctionBeginUser;

  int block, grid;
  block = 128;
  grid = 64;

  for(int i=0; i < num_iter; i++)
    for(int color=0; color < 4; color++)
    {
      //PetscViewer    viewer2;
      //PetscErrorCode ierr;
      //ierr = PetscViewerASCIIPrintf(viewer2, "num_iter: %d, color: %d.\n", num_iter, color);CHKERRQ(ierr);

      updateKernel<<<grid,block>>>(N, color, fLocal[0], uLocal[0]);
      // printf("---end----\n");
      cerr = cudaDeviceSynchronize(); CUDA_CHK(cerr);
    }

  PetscFunctionReturn(0);
}
